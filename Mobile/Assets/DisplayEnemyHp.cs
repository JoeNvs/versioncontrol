﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayEnemyHp : MonoBehaviour {

    private void Update()
    {
        GetComponent<Text>().text = "" + GetComponentInParent<EnemyObject>().GetHp();
    }
}
