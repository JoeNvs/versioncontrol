﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragonSlayerEffect : MonoBehaviour {
    int startTurn;
    int endTurn;

    private void Start()
    {
        //startTurn = transform.root.GetComponentInChildren<BattleClass>().GetTurn();
        endTurn = startTurn + 3;
        Ability();
    }


    void Update()
    {
        print("start " + startTurn);
        print("end " +endTurn);
        if(GetComponent<TurnCounter>().TurnHasChanged() && GetComponent<TurnCounter>().GetCurrentTurn() < endTurn)
        {
            Ability();
        }

        if (GetComponent<TurnCounter>().GetCurrentTurn() >= startTurn + 3)
        {
            Destroy(GetComponent<DragonSlayerEffect>());
            Destroy(GetComponent<TurnCounter>());
            Destroy(gameObject);
        }
    }

    void Ability()
    {
        EnemyObject[] e = transform.root.GetComponentsInChildren<EnemyObject>();

        for (int i = 0; i < e.Length; i++)
        {
            e[i].damageTaken += 5;
        }

        //transform.root.GetComponent<BattleClass>().defended = true;
    }
}
