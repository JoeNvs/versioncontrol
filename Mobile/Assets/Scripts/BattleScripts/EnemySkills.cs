﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySkills : MonoBehaviour {

    public List<GameObject> skills;

    //apply enemy skill to member target
    public void UseSkill()
    {
        BattleClass b = transform.root.GetComponentInChildren<BattleClass>();
        b.SetTarget(transform.root.GetComponentInChildren<PlayerObject>().gameObject);//set new target to first member

        Instantiate(skills[0], transform);//use skill in index zero
    }
    //
}
