﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Turn { playerTurn, enemyTurn};

public class BattleClass : MonoBehaviour
{
    Turn battleFlow;
    int turn;
    int selectedCard;
    int combo;
    int manaPool;

    GameObject selectedMember;
    GameObject targetedEnemy;

    bool enemyHighlightable;
    bool defended;
    bool usedAttackCard;

    bool enemyDied;
    GameObject killedEnemy;


    public BattleClass()
    {
        battleFlow = Turn.playerTurn;
        turn = 0;
        selectedCard = 0;
        combo = 0;
        manaPool = 0;

        selectedMember = null;//COME BACK TO THIS
        targetedEnemy = null;

        enemyHighlightable = false;
        defended = false;
        usedAttackCard = false;

        enemyDied = false;
        killedEnemy = null;
    }

    public BattleClass(Turn t)
    {
        battleFlow = t;
        turn = 0;
        selectedCard = 0;
        combo = 0;
        manaPool = 0;

        selectedMember = null;//COME BACK TO THIS
        targetedEnemy = null;

        enemyHighlightable = false;
        defended = false;
        usedAttackCard = false;

        enemyDied = false;
        killedEnemy = null;
    }
    //END CONSTRUCTORS

    public void SetTurn(Turn t)
    {
        battleFlow = t;
    }

    public void SetTurnNum(int n)
    {
        turn = n;
    }

    public void SetSelectedIndex(int n)
    {
        selectedCard = n;
    }

    public void SetCombo(int n)
    {
        combo = n;
    }

    public void SetTarget(GameObject newTarget)
    {
        selectedMember = newTarget;
    }

    public void SetTargetEnemy(GameObject newTarget)
    {
        targetedEnemy = newTarget;
    }

    public void SetEnemyHighlight(bool newBool)
    {
        enemyHighlightable = newBool;
    }

    public void SetDefended(bool newBool)
    {
        defended = newBool;
    }

    public void SetUsedAttack(bool newBool)
    {
        usedAttackCard = newBool;
    }

    public void SetEnemyDied(bool newBool)
    {
        enemyDied = newBool;
    }

    public void SetEnemyKilled(GameObject g)
    {
        killedEnemy = g;
    }

    public void SetManaPool(int n)
    {
        manaPool = n;
    }

    public Turn GetTurn()
    {
        return battleFlow;
    }

    public int GetTurnNum()
    {
        return turn;
    }

    public int GetSelectedIndex()
    {
        return selectedCard;
    }

    public int GetCombo()
    {
        return combo;
    }

    public GameObject GetTarget()
    {
        return selectedMember;
    }

    public GameObject GetTargetEnemy()
    {
        return targetedEnemy;
    }

    public bool GetEnemyHighlight()
    {
        return enemyHighlightable;
    }

    public bool GetDefended()
    {
        return defended;
    }

    public bool GetUsedAttack()
    {
        return usedAttackCard;
    }

    public bool GetEnemyDied()
    {
        return enemyDied;
    }

    public GameObject GetEnemyKilled()
    {
        return killedEnemy;
    }

    public int GetManaPool()
    {
        return manaPool;
    }
    //END GET/SET


    public void UseMana(int n)
    {
        manaPool -= n;
    }

    public void EnableEnemyButton()
    {
        EnemyObject[] e = GetComponentsInChildren<EnemyObject>();

        for (int i = 0; i < e.Length; i++)
        {
            e[i].gameObject.GetComponent<Button>().enabled = true;
        }

    }

    public void DisableEnemyButton()
    {
        EnemyObject[] e = GetComponentsInChildren<EnemyObject>();

        for (int i = 0; i < e.Length; i++)
        {
            e[i].gameObject.GetComponent<Button>().enabled = false;
        }

    }
    
    public void ProcessEnemyDamage()
    {
        //access and change hp in EnemyObject
        EnemyObject[] e = GetComponentsInChildren<EnemyObject>();
        for (int i = 0; i < e.Length; i++)
        {
            e[i].TakeDamage();
            e[i].SetDamage(0);
        }
        //

        //Destroy EnemyObjects of hp <= 0
        for (int i = 0; i < e.Length; i++)
        {
            if (e[i].GetHp() <= 0)
            {
                Destroy(e[i].transform.gameObject);
            }
        }
        //
    }


    public void ProcessMemberDamage()
    {
        //access and change hp in selectedMember
        selectedMember.GetComponent<PlayerObject>().TakeDamage();
        selectedMember.GetComponent<PlayerObject>().SetDamage(0);
        //
    }

    //

    public void ChangeTurn()
    {
        if (battleFlow == Turn.playerTurn)
        {
            enemyHighlightable = true;//make enemies highlitable
            battleFlow = Turn.enemyTurn;
            GetComponentInChildren<PlayerObject>().DestroyHandComponents();
            GetComponentInChildren<PlayerObject>().ClearEffects();
        }

        else
        {
            enemyHighlightable = false;//turn off enemy highlighting
            //enemies use their skills
            EnemyObject[] e = transform.root.GetComponentsInChildren<EnemyObject>();
            for (int i = 0; i < e.Length; i++)
            {
                e[i].GetComponent<EnemySkills>().UseSkill();
            }
            //
            battleFlow = Turn.playerTurn;
        }
    }

    public void IncrementCombo()
    {
        combo++;
    }
}