﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*JoseLuis Neves
 * 02/04/19
 * Affects the BattleClass in that it will produce the next turn to initiate
 * */
public class EndTurn : MonoBehaviour {

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(PassTurn);
    }

    void PassTurn()
    {
        BattleClass parentObject = transform.root.GetComponentInChildren<BattleClass>();
        //transform.root.GetComponentInChildren<BattleClass>().SetHandBool(false);
        HandSlotObject[] cards = transform.root.GetComponentsInChildren<HandSlotObject>();

        for(int i =0; i < cards.Length; i++)
        {
            //cards[i].gameObject.GetComponent<Animator>().SetTrigger("Reset");
        }

        parentObject.ChangeTurn();
        parentObject.SetUsedAttack(false);
    }
}
