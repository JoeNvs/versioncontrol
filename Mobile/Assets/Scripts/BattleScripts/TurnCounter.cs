﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnCounter : MonoBehaviour {

    int startTurn;
    int currentTurn;

    private void Start()
    {
        //startTurn = transform.root.GetComponent<BattleClass>().GetTurn();
    }

    private void Update()
    {
        print("current " + currentTurn);
        //currentTurn = transform.root.GetComponent<BattleClass>().GetTurn();
    }


    public bool TurnHasChanged()
    {
        if(currentTurn > startTurn)
        {
            startTurn = currentTurn;
            return true;
        }

        return false;
    }


    public int GetCurrentTurn()
    {
        return currentTurn;
    }

    public int GetStartTurn()
    {
        return startTurn;
    }

}
