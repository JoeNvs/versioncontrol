﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/*Drag selection implementations*/
public class TeamMemberSelect : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler{
    Vector3 startPos;
    Vector3 originalPos;
    
    void Start()
    {
        originalPos = transform.position;
    }

    public void OnBeginDrag(PointerEventData e)
    {
        startPos = transform.position;
    }

    public void OnDrag(PointerEventData e)
    {
        transform.position = e.position;
    }


    public void OnEndDrag(PointerEventData e)
    {
     }

}
