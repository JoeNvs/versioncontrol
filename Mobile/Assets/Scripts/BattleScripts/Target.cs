﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*JoseLuis Neves
 * 02/04/19
 * Places selected GameObject as target in BattleClass
 * */
public class Target : MonoBehaviour {

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(TargetThis);
    }

    void TargetThis()
    {
        print("New Target");
        transform.root.GetComponentInChildren<BattleClass>().SetTarget(this.gameObject);
    }
}
