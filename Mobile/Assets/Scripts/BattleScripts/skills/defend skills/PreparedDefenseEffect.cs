﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreparedDefenseEffect : MonoBehaviour {

    private void Start()
    {
        ClassStats m = transform.root.GetComponentInChildren<ClassStats>();
        m.ChangeHp(5);
        transform.root.GetComponentInChildren<BattleClass>().UseMana(this.GetComponent<CardObject>().manaCost);

        //transform.root.GetComponent<BattleClass>().defended = true;
    }
}
