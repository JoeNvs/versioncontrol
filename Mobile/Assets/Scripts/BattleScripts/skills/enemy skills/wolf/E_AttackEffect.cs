﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E_AttackEffect : MonoBehaviour {

    private void Start()
    {
        BattleClass parentObject = transform.root.GetComponentInChildren<BattleClass>();
        GetDefaultTarget(parentObject);

        //add damage from card object to damageTaken on target
        parentObject.GetTarget().GetComponent<PlayerObject>().SetDamage(parentObject.GetTarget().GetComponent<PlayerObject>().GetDamage() + GetComponent<CardObject>().GetDamage());
        parentObject.ProcessMemberDamage();
    }
    //END UNITY FUNCTIONS

    void GetDefaultTarget(BattleClass parent)
    {
        if (parent.GetTarget() == null)
            transform.root.GetComponentInChildren<BattleClass>().SetTarget(transform.root.GetComponentInChildren<PlayerObject>().gameObject);

    }
}
