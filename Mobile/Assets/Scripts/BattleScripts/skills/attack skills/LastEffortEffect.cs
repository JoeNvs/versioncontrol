﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastEffortEffect : MonoBehaviour {
    bool dragonPresent;

    private void Start()
    {
        dragonPresent = false;

        BattleClass parent = transform.root.GetComponentInChildren<BattleClass>();

        ClassStats m = transform.root.GetComponentInChildren<ClassStats>();
        parent.UseMana(this.GetComponent<CardObject>().manaCost);
        GetDefaultTarget(parent);

        EnemyObject[] enemies = transform.root.GetComponentsInChildren<EnemyObject>();

        for(int i = 0; i < enemies.Length; i++)
        {
            if(enemies[i].element == "dragon")
            {
                dragonPresent = true;
            }
        }

        if(dragonPresent)
        {
            List<GameObject> deck = m.GetComponentInChildren<DeckController>().GetComponentInChildren<DeckObject>().GetDeck();
            List<GameObject> attackCards = new List<GameObject>();

            for(int i = 0; i < deck.Count; i++)
            {
                if (deck[i].GetComponent<CardObject>().element == 'a')
                    attackCards.Add(deck[i]);
            }

            //DISPLAY ON THE SCREEN
        }//end if
    }

    void GetDefaultTarget(BattleClass parent)
    {
        if (parent.GetTarget() == null)
            transform.root.GetComponentInChildren<BattleClass>().SetTarget(transform.root.GetComponentInChildren<EnemyObject>().gameObject);

    }
}
