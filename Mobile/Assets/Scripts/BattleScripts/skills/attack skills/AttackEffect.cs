﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*JoseLuis Neves
 * 02/04/19
 * Contains the data for the Attack card skill
 * */
public class AttackEffect : MonoBehaviour {
    
    private void Start()
    {
        BattleClass parentObject = transform.root.GetComponentInChildren<BattleClass>();
        GetDefaultTarget(parentObject);

        parentObject.GetTargetEnemy().GetComponent<EnemyObject>().SetDamage(parentObject.GetTargetEnemy().GetComponent<EnemyObject>().damageTaken + GetComponent<CardObject>().GetDamage());

        parentObject.IncrementCombo();
        transform.root.GetComponentInChildren<ComboCounter>().combo++;

        //if combo count is at 3 deal damage to all enemies instead
        if((parentObject.GetCombo() % 3) == 0 && parentObject.GetCombo() > 0)
        {
            parentObject.GetTargetEnemy().GetComponent<EnemyObject>().SetDamage(0);
            EnemyObject[] e = transform.root.GetComponentsInChildren<EnemyObject>();

            for (int i = 0; i < e.Length; i++)
            {
                e[i].damageTaken += GetComponent<CardObject>().GetDamage();
            }
        }

        parentObject.SetUsedAttack(true);
        parentObject.ProcessEnemyDamage();
    }
    //END UNITY FUNCTIONS

    void GetDefaultTarget(BattleClass parent)
    {
        if (parent.GetTargetEnemy() == null)
            transform.root.GetComponentInChildren<BattleClass>().SetTargetEnemy(transform.root.GetComponentInChildren<EnemyObject>().gameObject);

    }
}
