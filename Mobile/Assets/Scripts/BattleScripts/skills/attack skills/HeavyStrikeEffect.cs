﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyStrikeEffect : MonoBehaviour {
    private int FOCUS_COST = 2;

    public void Start()
    {
        BattleClass parentObject = transform.root.GetComponentInChildren<BattleClass>();
        GetDefaultTarget(parentObject);

        //Deal damage to target Enemy
        if (Usable())
        {
            parentObject.GetTargetEnemy().GetComponent<EnemyObject>().SetDamage(GetComponent<CardObject>().GetDamage());
            //

            parentObject.SetUsedAttack(true);
            parentObject.ProcessEnemyDamage();
        }
    }

    void GetDefaultTarget(BattleClass parent)
    {
        if (parent.GetTargetEnemy() == null)
            transform.root.GetComponentInChildren<BattleClass>().SetTargetEnemy(transform.root.GetComponentInChildren<EnemyObject>().gameObject);

    }

    private bool Usable()
    {
        ClassStats m = transform.root.GetComponentInChildren<ClassStats>();
        return m.GetFocus() >= FOCUS_COST;
    }
}
