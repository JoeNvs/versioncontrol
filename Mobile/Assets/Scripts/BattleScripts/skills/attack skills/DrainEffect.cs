﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrainEffect : MonoBehaviour {

    private void Start()
    {
        BattleClass parentObject = transform.root.GetComponentInChildren<BattleClass>();
        if (parentObject.GetTarget() == null)
            transform.root.GetComponentInChildren<EnemyObject>().SetDamage(transform.root.GetComponentInChildren<EnemyObject>().damageTaken + GetComponent<CardObject>().GetDamage());

        else parentObject.GetTarget().GetComponent<EnemyObject>().SetDamage(parentObject.GetTarget().GetComponent<EnemyObject>().damageTaken + GetComponent<CardObject>().GetDamage());

    }
}
