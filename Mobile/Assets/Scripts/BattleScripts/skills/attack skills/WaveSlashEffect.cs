﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSlashEffect : MonoBehaviour {

    private void Start()
    {
        //
        BattleClass parentObject = transform.root.GetComponentInChildren<BattleClass>();
        EnemyObject[] e = transform.root.GetComponentsInChildren<EnemyObject>();

        //Deal damage to all enemies on scene
        for(int i = 0; i < e.Length; i++)
        {
            e[i].damageTaken += GetComponent<CardObject>().GetDamage();
        }
        //

        parentObject.UseMana(this.GetComponent<CardObject>().manaCost);
        parentObject.SetUsedAttack(true);
        parentObject.ProcessEnemyDamage();
    }
}
