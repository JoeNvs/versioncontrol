﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarmUpEffect : MonoBehaviour {

    private void Start()
    {
        CardObject c = transform.parent.GetComponentInChildren<CardObject>();
        c.SetDamage(c.GetDamage() + 5);

        ClassStats m = transform.root.GetComponentInChildren<ClassStats>();
        transform.root.GetComponentInChildren<BattleClass>().UseMana(this.GetComponent<CardObject>().manaCost);
    }
}
