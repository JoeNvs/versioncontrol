﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DragonBloodEffect : MonoBehaviour {

    private void Start()
    {
        BattleClass parent = transform.root.GetComponentInChildren<BattleClass>();

        parent.UseMana(this.GetComponent<CardObject>().manaCost);
        GetDefaultTarget(parent);

        if (parent.GetTarget().GetComponent<EnemyObject>().element == "dragon" && DragonDies())
        {
            parent.GetTarget().GetComponent<EnemyObject>().GetComponent<Button>().enabled = false;

            EnemyObject[] e = parent.GetComponentsInChildren<EnemyObject>();
            
            for (int i = 0; i < e.Length; i++)
            {
                e[i].GetComponent<Target>().enabled = false;
                e[i].gameObject.AddComponent<ExtraTargeting>();
                e[i].GetComponent<ExtraTargeting>().damage = 5;
            }
        }
        print("DragonBlood");
        //parent.enemyHighlitable = true;
    }//end start

    void GetDefaultTarget(BattleClass parent)
    {
        if (parent.GetTarget() == null)
            transform.root.GetComponentInChildren<BattleClass>().SetTarget(transform.root.GetComponentInChildren<EnemyObject>().gameObject);

    }

    bool DragonDies()
    {//MISSING CHECK FOR DRAGON THAT DIES
        EnemyObject[] e = transform.root.GetComponent<BattleClass>().GetComponentsInChildren<EnemyObject>();

        for(int i = 0; i < e.Length; i++)
        {
            if (e[i].willDie())
                return true;
        }

        return false;
    }
}



