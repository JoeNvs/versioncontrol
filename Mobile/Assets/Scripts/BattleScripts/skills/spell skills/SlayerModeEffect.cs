﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlayerModeEffect : MonoBehaviour {

    private void Start()
    {
        BattleClass parent = transform.root.GetComponentInChildren<BattleClass>();

        ClassStats m = transform.root.GetComponentInChildren<ClassStats>();
        parent.UseMana(this.GetComponent<CardObject>().manaCost);
        GetDefaultTarget(parent);

        if(parent.GetTarget().GetComponent<EnemyObject>().element == "dragon")
        {
            parent.GetTarget().GetComponent<EnemyObject>().SetDamage(parent.GetTarget().GetComponent<EnemyObject>().damageTaken + GetComponent<CardObject>().GetDamage());
        }

        //MISSING REDUCTION OF DRAGON ATTACKS

    }


    void GetDefaultTarget(BattleClass parent)
    {
        if (parent.GetTarget() == null)
            transform.root.GetComponentInChildren<BattleClass>().SetTarget(transform.root.GetComponentInChildren<EnemyObject>().gameObject);

    }
}
