﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*JoseLuis Neves
 * 02/04/19
 * Calculations for landing into battle during dungeon movement
 * */
public class EngagementOdds : MonoBehaviour
{
    public float levelDifficulty;
    [Range(0, 1)] public float odds;
    public string battleSceneName;

    // The lowest value that can be rolled to trigger an encounter.
    private float encounterLowerBound;

    // The highest value that can be rolled to trigger an encounter.
    private float encounterUpperBound;


    private void Start()
    {
        // The size of the non-encounter space on each side of the number line.
        float edgeSize = (1 - odds) * levelDifficulty / 2;
        encounterLowerBound = edgeSize;
        encounterUpperBound = levelDifficulty - edgeSize;
    }

    /// <summary>
    /// Determines if an encounter should occur, and launches one if it should.
    /// </summary>
    public void GenerateOdds()
    {
        float rand = Random.value * levelDifficulty;
        if (rand >= encounterLowerBound && rand < encounterUpperBound)
        {
            Debug.Log("Generated encounter with level " + rand);
            SceneManager.LoadScene(battleSceneName);
        }
    } //end GenerateOdds Method
}
