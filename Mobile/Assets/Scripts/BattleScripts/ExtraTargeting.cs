﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ExtraTargeting : MonoBehaviour
{
    public int damage;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(TargetThis);
    }

    void TargetThis()
    {
        print("New Target");
        transform.root.GetComponentInChildren<BattleClass>().SetTarget(this.gameObject);

        EnemyObject[] e = transform.root.GetComponentInChildren<BattleClass>().GetComponentsInChildren<EnemyObject>();
        transform.root.GetComponentInChildren<BattleClass>().GetTarget().GetComponent<EnemyObject>().SetDamage(damage);
        for (int i = 0; i < e.Length; i++)
        {
            e[i].GetComponent<Target>().enabled = true;
            Destroy(e[i].gameObject.GetComponent<ExtraTargeting>());
        }
        
    }

}
