﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor.SceneManagement;

public class SelectTeamMember : MonoBehaviour {
    public GameObject prefab;//assign prefab gameobject

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(AddMember);
    }

    void AddMember()
    {
        //static variable in TeamCardsObject will be assigned reference to prefab
        //t.SetMember(prefab);
        EditorSceneManager.LoadScene(1);
    }
}
