﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*JoseLuis Neves
 * 02/04/19
 * Holds a gameObject prefab for OnClick events to activate a selected card and its abilities
 * */
public class CardSelect : MonoBehaviour {

    GameObject card;

    public void SetCard(GameObject newCard)
    {
        card = newCard;
    }

    private void Start()
    {
        BattleClass parentObject = transform.root.GetComponentInChildren<BattleClass>();
        GetComponent<Button>().onClick.AddListener(InstantiateCardEffect);
        parentObject.SetEnemyHighlight(true);
    }

    void InstantiateCardEffect()
    {
        BattleClass parentObject = transform.root.GetComponentInChildren<BattleClass>();

        if (Usable())
        {
            parentObject.SetSelectedIndex(GetComponent<HandSlotObject>().slotNumber);
            
            //GetComponent<Animator>().SetTrigger("Middle");
            Instantiate(card, transform);
            GetComponentInParent<PlayerObject>().DestroyHandCards(parentObject.GetSelectedIndex());
        }
        
    }

    //If mana cost does not exceed current mana return true
    public bool Usable()
    {
        BattleClass b = transform.root.GetComponentInChildren<BattleClass>();
        return b.GetManaPool() >= card.GetComponent<CardObject>().manaCost;
    }
    //
}
