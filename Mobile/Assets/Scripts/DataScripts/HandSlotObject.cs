﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*JoseLuis Neves
 * 02/04/19
 * Contains data for the GameObject that contains the handslot positions
 * */
public class HandSlotObject : MonoBehaviour {
    Vector3 cardPosition;
    public int slotNumber;

    public Vector3 GetPosition()
    {
        return cardPosition;
    }

    public int GetSlot()
    {
        return slotNumber;
    }

    public GameObject GetHandSlot()
    {
        return gameObject;
    }

    public void SetPosition(Vector3 newPosition)
    {
        cardPosition = newPosition;
    }
    //END GET/SET
}
