﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ClassStats : MonoBehaviour {

    public int[] hp = new int[2];
    private int damageTaken;
    private int guard;
    public int focus;
    
    public ClassStats()
    {
        hp[0] = 0;
        hp[1] = 0;
        damageTaken = 0;
        guard = 0;
        focus = 0;
    }

    public ClassStats(int[] a)
    {
        hp[0] = a[0];
        hp[1] = a[1];
        damageTaken = 0;
        guard = 0;
        focus = 0;
    }
    //END CONSTRUCTORS

    public void SetHandObject(PlayerObject h)
    {
        hp[0] = h.hp[0];
        hp[1] = h.hp[1];
        damageTaken = 0;
        guard = 0;
        focus = 0;
    }

    public int GetHp()
    {
        return hp[0];
    }

    public int GetDamage()
    {
        return damageTaken;
    }

    public int GetGuard()
    {
        return guard;
    }

    public int GetFocus()
    {
        return focus;
    }

    public void SetHp(int n)
    {
        hp[0] = n;
    }

    public void SetMaxHP(int n)
    {
        hp[1] = n;
    }

    public void SetDamage(int n)
    {
        damageTaken = n;
    }

    public void SetGuard(int n)
    {
        guard = n;
    }

    public void SetFocus(int n)
    {
        focus = n;
    }
    //END GET/SET

    public void TakeDamage()
    {
        hp[0] -= (damageTaken - guard);
    }
    
    public void ChangeHp(int n)
    {
        hp[0] += n;
    }

    public void Copy(ClassStats m)
    {
        hp = m.hp;
        focus = m.focus;
    }
}
