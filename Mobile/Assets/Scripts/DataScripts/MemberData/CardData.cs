﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * holds card GameObejct and copy amount for individual card
 * */

public class CardData {

    GameObject card;
    int copies;

    public CardData()
    {
        card = null;
        copies = 0;
    }

    public CardData(GameObject g, int n)
    {
        card = g;
        copies = n;
    }
    //END CONSTRUCTOR

    public void SetCard(GameObject g)
    {
        card = g;
    }

    public void SetCopies(int n)
    {
        copies = n;
    }

    public GameObject GetCard()
    {
        return card;
    }

    public int GetCopies()
    {
        return copies;
    }
    //END GET/SET
}
