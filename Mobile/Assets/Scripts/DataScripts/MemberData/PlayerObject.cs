﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*JoseLuis Neves
 * 02/04/19
 * Creates the display for cards(GameObjects) in the players hand.
 * */
public class PlayerObject : DeckObject {
    public Font font;
    List<Transform> cardSlots;
    private bool handGenerated;

    public PlayerObject() : base()
    {

    }

    public PlayerObject(int[] a, List<CardData> list) : base(a, list)
    {

    }

    //END CONSTRUCTORS

    private void Start()
    {
        SetDamage(0);
    }

    //END UNITY FUNCTIONS

    public void SetSlots(Transform[] t)
    {
        for(int i = 0; i < t.Length; i++)
        {
            cardSlots[i] = t[i];
        }
    }

    public List<Transform> GetSlots()
    {
        return cardSlots;
    }

    //END GET/SET

    //get gameobjects in heirarchy for displaying cards to player
    public void FindCardSlots()
    {
        HandSlotObject[] h = GetComponentsInChildren<HandSlotObject>();

        for (int i = 0; i < h.Length; i++)
        {
            if (h[i].GetComponent<Image>() == null)
            {
                cardSlots.Add(h[i].GetHandSlot().transform);
            }
        }

        //there can be no more card slots than cards in deck
        while (cardSlots.Count > GetDeck().Count)
            cardSlots.RemoveAt(cardSlots.Count - 1);
    }
    //

    //destroy display elements for cards
    public void DestroyHandComponents()
    {
        for (int i = 0; i < cardSlots.Count; i++)
        {
            if (!(cardSlots[i].GetComponentInChildren<Text>() == null))//only the activated skill will have a child object in the hieracrchy
            {
                Destroy(cardSlots[i].gameObject.GetComponent<Image>());
                Destroy(cardSlots[i].gameObject.GetComponent<Button>());
                Destroy(cardSlots[i].gameObject.GetComponent<CardSelect>());
                Destroy(cardSlots[i].GetComponentInChildren<Text>().gameObject);
            }
        }

        handGenerated = false;
    }
    //

    //add necessary components to display hand to player
    public void GenerateHandComponents()
    {
        cardSlots = new List<Transform>();
        FindCardSlots();
        //Add image and button to display cards
        if (!handGenerated)
        {
            for (int i = 0; i < cardSlots.Count; i++)
            {
                cardSlots[i].gameObject.AddComponent<Image>();
                cardSlots[i].gameObject.AddComponent<Button>();

                GameObject child = Instantiate(new GameObject(), cardSlots[i]);
                child.AddComponent<Text>();
                child.GetComponent<Text>().font = font;
                child.GetComponent<Text>().color = Color.black;
                child.GetComponent<Text>().resizeTextForBestFit = true;
                child.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
                child.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
                child.GetComponent<Text>().resizeTextMinSize = 25;

                child.GetComponent<Text>().text = GetDeck()[i].name;
                cardSlots[i].gameObject.AddComponent<CardSelect>();//Assign onClick function through CardSelect
                cardSlots[i].gameObject.GetComponent<CardSelect>().SetCard(GetDeck()[i]);//assign prefab to CardSelect
                cardSlots[i].name = GetDeck()[i].name;
            }

            handGenerated = true;
        }
        //
    }
    //

    //override of DeckObject method--destroy a card from deck and hand
    public void DestroyHandCards(int n)
    {
        //Destroy card in deck then remove card from hand
        base.DestroyHandCards(n);
        Destroy(cardSlots[n].GetComponentInChildren<Text>().gameObject);
        Destroy(cardSlots[n].gameObject.GetComponent<Image>());
        Destroy(cardSlots[n].gameObject.GetComponent<Button>());
        Destroy(cardSlots[n].gameObject.GetComponent<CardSelect>());
        //

        handGenerated = false;//hand no longer generated
        isActive = false;//deactivate player button
    }
    //

    //Get rid of loose card effect gameobjects on scene hierarchy
    public void ClearEffects()
    {
        for (int i = 0; i < cardSlots.Count; i++)
        {
            CardObject[] g = cardSlots[i].GetComponentsInChildren<CardObject>();
            for (int j = 0; j < g.Length; j++)
            {
                Destroy(g[j].gameObject);
            }
        }
    }
    //

    public void ButtonToggle(bool b)
    {
        GetComponent<Button>().enabled = b;
    }

    public void Copy(PlayerObject h)
    {
        base.Copy(h);
        font = h.font;
    }
}
