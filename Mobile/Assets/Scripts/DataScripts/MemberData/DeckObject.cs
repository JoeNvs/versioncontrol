﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*JoseLuis Neves
 * 02/04/19
 * The Object that contains the data for the GameObjects in a deck
 * */
public abstract class DeckObject : DeckController {

    List<GameObject> deck;

    public DeckObject() : base()
    {
        deck = new List<GameObject>();
    }

    public DeckObject(int[] a, List<CardData> list) : base(a, list)
    {
        GenerateDeck();
    }

    public DeckObject(List<GameObject> newDeck)
    {
        deck = new List<GameObject>();

        for(int i = 0; i < newDeck.Count; i++)
        {
            deck.Add(newDeck[i]);
        }
    }
    //END CONSTRUCTORS

    public void SetHandObject(PlayerObject h)
    {
        base.SetHandObject(h);
        GenerateDeck();
    }

    public void SetDeck(List<CardData> list)
    {
        base.SetCards(list);
        GenerateDeck();
    }

    public List<GameObject> GetDeck()
    {
        return deck;
    }
    //END GET/SET

    //create a list of cards using the appropriate amount of copies assigned
    public void GenerateDeck()
    {
        deck = new List<GameObject>();

        for (int i = 0; i < GetCards().Count; i++)
        {
            for (int j = 0; j < GetCards()[i].GetCopies(); j++)
            {
                deck.Add(cards[i].GetCard());//cards is a public reference from DeckController
            }
        }
    }
    //

    //create a list of cards using the appropriate amount of copies assigned from parameters
    public void GenerateDeck(List<GameObject> cards, List<int> copy)
    {
        deck = new List<GameObject>();

        for (int i = 0; i < cards.Count; i++)
        {
            for (int j = 0; j < copy[i]; j++)
            {
                deck.Add(cards[i]);//cards from parameters
            }
        }
    }
    //

    //Randomly organize the deck
    public void Shuffle()
    {
        List<GameObject> temp = new List<GameObject>();

        for(int i = 0; i < deck.Count; i++)
        {
            temp.Add(deck[i]);
        }

        for(int i = 0; i < deck.Count; i++)
        {
            int n = Random.Range(0, temp.Count);

            deck[i] = temp[n];
            temp.RemoveAt(n);
        }
    }
    //

    //destroy deck element n
    public void DestroyHandCards(int n)
    {
        deck.RemoveAt(n);
    }
    //

    public void Copy(DeckObject d)
    {
        base.Copy(d);
        deck = d.deck;
    }
}
