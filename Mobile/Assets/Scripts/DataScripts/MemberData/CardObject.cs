﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*JoseLuis Neves
 * 02/04/19
 * Object that contains the data for a cards element and damage(if any)
 * NOT AN OBJECT IN THE HIERARCHY ONLY REFERENCED
 * */
public class CardObject : MonoBehaviour {

    public int damage;
    public int manaCost;
    public char element;

    public int GetDamage()
    {
        return damage;
    }

    public void SetDamage(int newDamage)
    {
        damage = newDamage;
    }

}
