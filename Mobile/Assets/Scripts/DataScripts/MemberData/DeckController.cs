﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*JoseLuis Neves
 * 02/04/19
 * Holds Deck card elements
 * */
public abstract class DeckController : ClassStats
{

    public List<CardData> cards;
    public bool isActive;

    public DeckController() : base()
    {
        cards = new List<CardData>();
        isActive = false;
    }

    public DeckController(int[] a, List<CardData> list) : base(a)
    {
        cards = list;
        isActive = false;
    }
    //END CONSTRUCTORS

    public void SetHandObject(PlayerObject h)
    {
        base.SetHandObject(h);
        cards = h.cards;
    }

    public void SetCards(List<CardData> c)
    {
        cards = new List<CardData>();

        for(int i = 0; i < c.Count; i++)
        {
            cards.Add(c[i]);
        }
    }

    public void SetActive(bool newBool)
    {
        isActive = newBool;
    }

    public List<CardData> GetCards()
    {
        return cards;
    }

    public bool GetActive()
    {
        return isActive;
    }
    //END GET/SET

    public void SetNewCards(List<CardData> c)
    {
        for (int i = 0; i < c.Count; i++)
        {
            cards.Add(c[i]);
        }
    }

    public void Copy(DeckController d)
    {
        base.Copy(d);
        cards = d.cards;
    }
}//end class
