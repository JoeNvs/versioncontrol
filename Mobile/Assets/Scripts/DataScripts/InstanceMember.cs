﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstanceMember : MonoBehaviour {

    private void Start()
    {
        //Create an instance of the dataController's PlayerObject
        gameObject.AddComponent<PlayerObject>();
        GetComponent<PlayerObject>().Copy(DataController.member);
        GetComponent<PlayerObject>().Shuffle();
        GetComponent<Button>().onClick.AddListener(GenerateHandComponents);
        //
    }

    void GenerateHandComponents()
    {
        GetComponent<PlayerObject>().GenerateHandComponents();
    }
}
