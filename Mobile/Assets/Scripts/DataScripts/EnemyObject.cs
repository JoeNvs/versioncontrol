﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*JoseLuis Neves
 * 02/04/19
 * Contains data for enemy GameObjects
 * */
public class EnemyObject : MonoBehaviour {

    public int[] hp = new int[2];
    public int damageTaken;
    public string element = "dragon";
    private int guard;

    //

    private void Start()
    {
        damageTaken = 0;
        guard = 0;
    }
    //END UNITY FUNCTIONS

    public int GetHp()
    {
        return hp[0];
    }

    public int GetGuard()
    {
        return guard;
    }

    public void SetHp(int n)
    {
        hp[0] = n;
    }

    public void SetDamage(int n)
    {
        damageTaken = n;
    }

    public void SetGuard(int n)
    {
        guard = n;
    }
    //END GET/SET

    public void TakeDamage()
    {
        hp[0] -= (damageTaken - guard);
    }

    public bool willDie()
    {
        return hp[0] - damageTaken <= 0;
    }
}
