﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPathsGeneration : MonoBehaviour {

    private GameObject pathPrefab;
    private GameObject currentParent;
    private int tilesGenerated;


    public RandomPathsGeneration()
    {
        tilesGenerated = 0;
        currentParent = null;
        pathPrefab = null;
    }

    public RandomPathsGeneration(GameObject g, GameObject parent)
    {
        tilesGenerated = 0;
        currentParent = parent;
        pathPrefab = g;
    }

    public void Initialize(GameObject g, GameObject parent)
    {
        currentParent = parent;
        pathPrefab = g;
    }
    //END CONSTRUCTORS//

    public GameObject GetCurrentParent()
    {
        return currentParent;
    }
    
    //END GET/SET//
    public int TilesGenerated()
    {
        return tilesGenerated;
    }

    public void PlacePathBack(int n)
    {
        tilesGenerated = 0;

        List<GameObject> newPath = new List<GameObject>();

        for (int i = 0; i < n; i++)
        {
            newPath.Add(Instantiate(pathPrefab));
            newPath[i].transform.position = currentParent.transform.position + new Vector3(0, 0, 2);
            currentParent = newPath[i];
            tilesGenerated++;
        }
    }

    public void PlacePathFront(int n)
    {
        tilesGenerated = 0;

        List<GameObject> newPath = new List<GameObject>();

        for (int i = 0; i < n; i++)
        {
            newPath.Add(Instantiate(pathPrefab));
            newPath[i].transform.position = currentParent.transform.position + new Vector3(0, 0, -2);
            currentParent = newPath[i];
            tilesGenerated++;
        }
    }

    public void PlacePathLeft(int n)
    {
        tilesGenerated = 0;

        List<GameObject> newPath = new List<GameObject>();

        for (int i = 0; i < n; i++)
        {
            newPath.Add(Instantiate(pathPrefab));
            newPath[i].transform.position = currentParent.transform.position + new Vector3(2, 0, 0);
            currentParent = newPath[i];
            tilesGenerated++;
        }
    }

    public void PlacePathRight(int n)
    {
        tilesGenerated = 0;

        List<GameObject> newPath = new List<GameObject>();

        for (int i = 0; i < n; i++)
        {
            newPath.Add(Instantiate(pathPrefab));
            newPath[i].transform.position = currentParent.transform.position + new Vector3(-2, 0, 0);
            currentParent = newPath[i];
            tilesGenerated++;
        }
    }
}
