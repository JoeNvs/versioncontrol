﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonGenerationClient : MonoBehaviour {
    //public GameObject prefab;
    public GameObject icon;
    GameObject g;
    DungeonGeneration dungeon;
    int x;
    int y;
    int dungeonX;
    int dungeonY;

    // Use this for initialization
    void Start () {
        gameObject.AddComponent<DungeonGeneration>();
        dungeon = GetComponent<DungeonGeneration>();
        dungeonX = dungeon.GetX();
        dungeonY = dungeon.GetY();
        x = dungeon.GetX();
        y = dungeon.GetY();
        dungeon.CreateDungeonGeneration(50);
        dungeon.CreatePath(3);
        dungeon.CreateRoom(3);
        dungeon.CreatePath(5);
        dungeon.CreateRoom(5);
        g = Instantiate(icon, GetComponent<RectTransform>());
    }

    private void Update()
    {
        g.GetComponent<RectTransform>().anchoredPosition = new Vector3(x, y, 0);
    }

    public void MoveLeft()
    {
        if (dungeonX > 0 && dungeon.CheckBounds(dungeonX - 1, dungeonY, true))
        {
            x -= 50;
            dungeonX--;
        }
    }

    public void MoveRight()
    {
        if (dungeonX < dungeon.GetDimension() - 1 && dungeon.CheckBounds(dungeonX + 1, dungeonY, true))
        {
            x += 50;
            dungeonX++;
        }
    }

    public void MoveForward()
    {
        if (dungeonY < dungeon.GetDimension() - 1 && dungeon.CheckBounds(dungeonY + 1, dungeonX, false))
        {
            y += 50;
            dungeonY++;
        }
    }

    public void MoveDownward()
    {
        if (dungeonY > 0 && dungeon.CheckBounds(dungeonY - 1, dungeonX, false))
        {
            y -= 50;
            dungeonY--;
        }
    }
    // Update is called once per frame
}
