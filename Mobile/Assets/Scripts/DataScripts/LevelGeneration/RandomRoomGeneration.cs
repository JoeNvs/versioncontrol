﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRoomGeneration : MonoBehaviour {
    private GameObject pathPrefab;
    private GameObject[,] room;
    private GameObject currentParent;
    private int tilesGenerated;
    private Vector3 initialTilePos;

    public void Initialize(GameObject g, GameObject parent)
    {
        tilesGenerated = 0;
        currentParent = parent;
        pathPrefab = g;
    }
    //END CONSTRUCTORS//

    public GameObject GetCurrentParent()
    {
        return currentParent;
    }

    //END GET/SET//

    public int TilesGenerated()
    {
        return tilesGenerated;
    }

    public void CreateRoom(int n, Direction d)
    {
        tilesGenerated = 0;
        room = new GameObject[n, n];
        initialTilePos = currentParent.transform.position;

        Vector3 placeHorizontal = new Vector3(-2, 0, 0);
        Vector3 placeVertical = new Vector3(0, 0, -2);

        //If placing room leftward or backward apply negative modifier to placeHorizontal/placeVertical to generate in correct direction
        switch(d)
        {
            case Direction.Backward:
                placeVertical *= -1;
                break;

            case Direction.Left:
                placeHorizontal *= -1;
                break;
        }

        //generate room tiles
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                room[i, j] = Instantiate(pathPrefab);
                room[i, j].transform.position = currentParent.transform.position;

                if (d == Direction.Forward || d == Direction.Backward)
                {
                    if (j != 0) room[i, j].transform.position += placeHorizontal;
                    else
                    {
                        room[i, j].transform.position = initialTilePos + placeVertical;
                        initialTilePos = room[i, j].transform.position;
                    }
                }

                else if (d == Direction.Right || d == Direction.Left)
                {
                    if (j != 0) room[i, j].transform.position += placeVertical;
                    else
                    {
                        room[i, j].transform.position = initialTilePos + placeHorizontal;
                        initialTilePos = room[i, j].transform.position;
                    }
                }

                currentParent = room[i, j];
                tilesGenerated++;
            }
        }

        GameObject g = Instantiate(pathPrefab);
        Vector3 v = new Vector3();
        switch (d)
        {
            case Direction.Forward:
                g.transform.position = room[0, (n / 2)].transform.position + new Vector3(0, 0, -2);
                v = new Vector3(0, 0, -2);
                break;
            case Direction.Backward:
                g.transform.position = room[n - 1, (n / 2)].transform.position + new Vector3(0, 0, 2);
                v = new Vector3(0, 0, 2);
                break;

            case Direction.Right:
                g.transform.position = room[(n / 2), n - 1].transform.position + new Vector3(-2, 0, 0);
                v = new Vector3(-2, 0, 0);
                break;

            case Direction.Left:
                g.transform.position = room[(n / 2), 0].transform.position + new Vector3(2, 0, 0);
                v = new Vector3(2, 0, 0);
                break;
        }
        currentParent = g;
        
        GameObject padding = Instantiate(pathPrefab);
        padding.transform.position = currentParent.transform.position + v;
        currentParent = padding;
    }
}
