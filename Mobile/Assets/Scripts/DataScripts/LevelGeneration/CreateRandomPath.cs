﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction {Forward, Backward, Left, Right}

public class CreateRandomPath : MonoBehaviour {
    private const int MAX_TILE = 10; 
    public GameObject tile;
    private Direction pathDir;
    private List<RandomPathsGeneration> path;
    private GameObject currentTile;
    int numOfPaths;
    int totalTiles;
    int[,] map;
    int mapIndexHorizontal;
    int mapIndexVertical;

    // Use this for initialization
    void Start () {
        map = new int[MAX_TILE, MAX_TILE];
        mapIndexHorizontal = MAX_TILE / 2;
        mapIndexVertical = MAX_TILE / 2;

        path = new List<RandomPathsGeneration>();
        numOfPaths = Random.Range(8, 10);
        currentTile = gameObject;
        Instantiate(tile);
        map[mapIndexVertical, mapIndexHorizontal] = 1;

        for(int i = 0; i < numOfPaths; i++)
        {
            CreatePath();
            CreateRoom();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void CreateRoom()
    {
        int scale = Random.Range(1, 3);

        GameObject g = new GameObject();
        g.AddComponent<RandomRoomGeneration>();
        g.GetComponent<RandomRoomGeneration>().Initialize(tile, currentTile);
        RandomRoomGeneration generatedRoom = g.GetComponent<RandomRoomGeneration>();

        int n = scale % 2 != 0 ? scale : scale + 1;
        switch (pathDir)
        {
            case Direction.Forward:
                print("Room Forward");
                generatedRoom.CreateRoom(n, Direction.Forward);
                break;

            case Direction.Backward:
                print("Room Backward");
                generatedRoom.CreateRoom(n, Direction.Backward);
                break;

            case Direction.Right:
                print("Room Rightward");
                generatedRoom.CreateRoom(n, Direction.Right);
                break;

            case Direction.Left:
                print("Room Leftward");
                generatedRoom.CreateRoom(n, Direction.Left);
                break;

        }
        currentTile = generatedRoom.GetCurrentParent();
    }


    private void CreatePath()
    {
        //decide random number of tiles
        int numOfTiles = (totalTiles < Mathf.Pow(MAX_TILE, 2)) ? Random.Range(2, 6) : 0;

        if (numOfTiles > 0)
        {
            //create new gameObject for path
            GameObject g = new GameObject();
            g.AddComponent<RandomPathsGeneration>();
            g.GetComponent<RandomPathsGeneration>().Initialize(tile, currentTile);
            RandomPathsGeneration generatedPath = g.GetComponent<RandomPathsGeneration>();
            int n = Random.Range(1, 5);
            //decide random direction for path to follow
            switch (n)
            {
                case 1:
                    mapIndexVertical = (mapIndexVertical > 0) ? mapIndexVertical - 1 : mapIndexVertical;

                    if (pathDir != Direction.Forward && pathDir != Direction.Backward)
                    {
                        print("Path forward: " + numOfTiles);
                        generatedPath.PlacePathFront(numOfTiles);
                        pathDir = Direction.Forward;

                        map[mapIndexHorizontal, mapIndexVertical] = 1;
                        totalTiles += generatedPath.TilesGenerated();

                    }

                    else
                    {
                        print("AltPath right: " + numOfTiles);
                        generatedPath.PlacePathRight(numOfTiles);
                        pathDir = Direction.Right;
                        totalTiles += generatedPath.TilesGenerated();
                    }
                    break;

                case 2:
                    mapIndexVertical = (mapIndexVertical < MAX_TILE - 1) ? mapIndexVertical + 1 : mapIndexVertical;

                    if (pathDir != Direction.Backward && pathDir != Direction.Forward)
                    {
                        print("Path backward: " + numOfTiles);
                        generatedPath.PlacePathBack(numOfTiles);
                        pathDir = Direction.Backward;

                        map[mapIndexHorizontal, mapIndexVertical] = 1;
                        totalTiles += generatedPath.TilesGenerated();

                    }

                    else
                    {
                        print("AltPath left: " + numOfTiles);
                        generatedPath.PlacePathLeft(numOfTiles);
                        pathDir = Direction.Left;
                        totalTiles += generatedPath.TilesGenerated();
                    }
                    break;

                case 3:
                    mapIndexHorizontal = (mapIndexHorizontal < MAX_TILE - 1) ? mapIndexHorizontal + 1 : mapIndexHorizontal;

                    if (pathDir != Direction.Right && pathDir != Direction.Left)
                    {
                        print("Path right: " + numOfTiles);
                        generatedPath.PlacePathRight(numOfTiles);
                        pathDir = Direction.Right;

                        map[mapIndexHorizontal, mapIndexVertical] = 1;
                        totalTiles += generatedPath.TilesGenerated();
                    }

                    else
                    {
                        print("AltPath backward: " + numOfTiles);
                        generatedPath.PlacePathBack(numOfTiles);
                        pathDir = Direction.Backward;
                        totalTiles += generatedPath.TilesGenerated();
                    }
                    break;

                case 4:
                    mapIndexHorizontal = (mapIndexHorizontal > 0) ? mapIndexHorizontal - 1 : mapIndexHorizontal;

                    if (pathDir != Direction.Left && pathDir != Direction.Right)
                    {
                        print("Path left: " + numOfTiles);
                        generatedPath.PlacePathLeft(numOfTiles);
                        pathDir = Direction.Left;
 
                        map[mapIndexHorizontal, mapIndexVertical] = 1;
                        totalTiles += generatedPath.TilesGenerated();
                    }

                    else
                    {
                        print("AltPath forward: " + numOfTiles);
                        generatedPath.PlacePathFront(numOfTiles);
                        pathDir = Direction.Forward;
                        totalTiles += generatedPath.TilesGenerated();
                    }
                    break;

            }//end switch
            currentTile = generatedPath.GetCurrentParent();
        }
    }
}
