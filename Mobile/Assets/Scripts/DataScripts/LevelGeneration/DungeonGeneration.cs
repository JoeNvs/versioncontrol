﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DungeonGeneration : MonoBehaviour {
    private GameObject currentTile;
    private int[,] dungeon;
    private int dimension;
    private int currentX;
    private int currentY;

    public void CheckDungeon()
    {
        for (int i = 0; i < dimension; i++)
        {
            for (int f = 0; f < dimension; f++)
            {
                print("dungeon: [" + i + "], [" + f + "] = " + dungeon[i, f]);
            }
        }
    }
    public DungeonGeneration()
    {
        dimension = 0;
        dungeon = new int[0, 0];
        currentX = 0;
        currentY = 0;
    }

    public void CreateDungeonGeneration(int x)
    {
        dimension = x;
        dungeon = new int[dimension, dimension];
        currentX = 0;
        currentY = 0;
    }

    public int GetX()
    {
        return currentX;
    }

    public int GetY()
    {
        return currentY;
    }

    public int GetDimension()
    {
        return dimension;
    }

    private void CreateTile(int x, int y)
    {
        x = x >= dimension ? dimension - 1 : (x = x < 0 ? 0 : x);
        y = y >= dimension ? dimension - 1 : (y = y < 0 ? 0 : y);

        dungeon[y, x] = 1;

        //create new gameObject(tile) for path
        GameObject g = new GameObject();
        g.transform.SetParent(GetComponent<RectTransform>());
        g.transform.rotation = transform.rotation;
        g.transform.localScale = new Vector3(1,1,1);
        
        g.AddComponent<RectTransform>();
        g.AddComponent<CanvasRenderer>();
        g.AddComponent<Image>();

        RectTransform gTransform = g.GetComponent<RectTransform>();
        gTransform.anchorMin = new Vector3(0, 0.5f);
        gTransform.anchorMax = new Vector3(0, 0.5f);
        gTransform.sizeDelta = new Vector2(50, 50);
        gTransform.pivot = new Vector2(0, 0.5f);
        gTransform.localPosition = new Vector3((50 * x) + 10, (50 * y), 0.0f);
        gTransform.anchoredPosition = new Vector2((50*x) + 10, (50*y));

    }

    public void CreatePath(int size)
    {
        int temp = currentX;
        for (int i = temp; i < temp + size; i++)
        {
            CreateTile(i, currentY);
            GenerateRight();
        }
    }

    public void CreateRoom(int dimension)
    {
        int tempX = currentX;
        int tempY = currentY;

        for(int i = tempY; i < tempY + dimension; i++)
        {
            for(int j = tempX; j < tempX + dimension; j++)
            {
                CreateTile(j, i);
                GenerateRight();
            }
            GenerateForward();
        }

        currentX = tempX + dimension - 1;
        currentY = tempY + dimension - 1;
    }

    public void CreateRoom(int dimension, int x, int y)
    {
        currentX = x;
        currentY = y;

        for (int i = currentY; i < currentY + dimension; i++)
        {
            for (int j = currentX; j < currentX + dimension; j++)
            {
                CreateTile(j,i);
            }
        }

        currentY += dimension - 1;
        currentX += dimension - 1;
        GenerateRight();
    }

    public void GenerateRight()
    {
        currentX++;
    }

    public void GenerateLeft()
    {
        currentX--;
    }

    public void GenerateForward()
    {
        currentY++;
    }

    public void GenerateBackward()
    {
        currentY--;
    }
    
    public bool CheckBounds(int nextMove, int altCoord, bool xCoord)
    {
        if (xCoord)
        {
            if (dungeon[altCoord, nextMove] != 1)
                return false;
        }

        else
            if (dungeon[nextMove, altCoord] != 1)
                return false;

        return true;
    }
}//end class
