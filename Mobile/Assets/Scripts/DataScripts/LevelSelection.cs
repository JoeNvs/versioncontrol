﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelection : MonoBehaviour {
    public GameObject[] prefabs;

    private void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(SelectSkill);
    }

    void SelectSkill()
    {
        //Set color of image to fully solid
        Color temp = GetComponent<Image>().color;
        temp.a = 1f;
        GetComponent<Image>().color = temp;
        //

        //make new set of card data to add to DataController
        CardData[] c = new CardData[prefabs.Length];
        for (int i = 0; i < c.Length; i++)
        {
            c[i] = new CardData(prefabs[i], 4);
        }
        transform.root.GetComponent<DataController>().AddCards(c);
        //
    }

}
