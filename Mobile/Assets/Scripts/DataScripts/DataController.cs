﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DataController : MonoBehaviour {
    List<CardData> cards;

    public static PlayerObject member;//static PlayerObject to be passed between scenes
    
    public void SetCards(CardData[] c)
    {
        cards = new List<CardData>();
        for (int i = 0; i < c.Length; i++)
        {
            cards.Add(c[i]);
        }
    }

    public List<CardData> GetCards()
    {
        return cards;
    }
    //END GET/SET

    private void Start()
    {
        cards = new List<CardData>();
        member = GetComponent<PlayerObject>();
    }

    public void AddCards(CardData[] c)
    {
        for (int i = 0; i < c.Length; i++)
        {
            cards.Add(c[i]);
        }

        member.SetNewCards(cards);
        member.GenerateDeck();
        //member.Shuffle();
    }

    public void Done()
    {
        SceneManager.LoadScene(1);
    }
}
