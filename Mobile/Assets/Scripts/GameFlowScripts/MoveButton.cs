﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveButton : MonoBehaviour
{
    public float direction;
    private Button button;
    private PlayerController controller;

    void Start()
    {
        controller = GetComponentInParent<PlayerController>();
        button = GetComponent<Button>();
    }

    void Update()
    {
        button.interactable = controller.GetState() == PlayerController.State.Idle && controller.CanMove(direction);
    }

    public void Move()
    {
        controller.MoveOneUnit(direction);
    }
}