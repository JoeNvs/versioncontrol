﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*JoseLuis Neves
 * Camera Controller_Movement Inputs Etc.
 * */

public class Controller : MonoBehaviour {

    private void Start()
    {
        StartCoroutine(Move());
    }

    IEnumerator Move()
    {
        while(true)
        {
            if(Input.GetKey("w"))
            {
                MoveOneUnit(this.gameObject);
            }

            yield return new WaitForSeconds(1);

        }
    }


    void MoveOneUnit(GameObject target)
    {
        target.GetComponent<Transform>().position += new Vector3(0, 0, 1);
    }
}
