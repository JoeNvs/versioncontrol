﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraTrigger : MonoBehaviour {

    Animator anim;
    Button button;

    private void Start()
    {
        anim = GetComponent<Animator>();
        button = GetComponentInChildren<Button>();
        button.onClick.AddListener(ChangeDirection);
    }

    Vector3 GetDirection(float camAngle)
    {
        //Getting subjective direction of the reference
        float x = Mathf.Sin(camAngle);
        float z = Mathf.Cos(camAngle);

        return new Vector3(x, 0, z);
    }


    void ChangeDirection()
    {
        // button.transform.GetComponent<MoveButton>().ChangeCamera(anim);
    }
}
