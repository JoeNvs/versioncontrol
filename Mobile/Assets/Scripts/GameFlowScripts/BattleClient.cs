﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BattleClient : MonoBehaviour {
    BattleClass b;


    public void Start()
    {
        b = transform.root.GetComponentInChildren<BattleClass>();
        b.SetManaPool(50);
    }

    public void Update()
    {
        //Change enemy button states to highlightable or not
        EnemyObject[] e = transform.root.GetComponentsInChildren<EnemyObject>();
        for(int i = 0; i < e.Length; i++)
        {
            e[i].gameObject.GetComponent<Button>().enabled = b.GetEnemyHighlight();
        }
        //

        //Change member button states to highlightable or not
        GetComponentInChildren<PlayerObject>().ButtonToggle(!(b.GetUsedAttack()));
        //

        //Hide hand cards if attack is used on member
        if (b.GetUsedAttack())
            GetComponentInChildren<PlayerObject>().DestroyHandComponents();
        //

        //Game Over Testing
        if (GetComponentInChildren<PlayerObject>().GetHp() <= 0)
            SceneManager.LoadScene("GAMEOVER");
        //

        //Game won testing
        int numberOfEnemies = transform.root.GetComponentsInChildren<EnemyObject>().Length;

        if (numberOfEnemies == 0)
            SceneManager.LoadScene("WIN");
        //

        //enemy turn processing
        if(b.GetTurn() == Turn.enemyTurn)
        {
            b.ChangeTurn();
        }
    }
}
