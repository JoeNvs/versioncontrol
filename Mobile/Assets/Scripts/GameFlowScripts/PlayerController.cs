using System;
using UnityEngine;

// TODO setup Animator?
public class PlayerController : MonoBehaviour
{
    [Tooltip("How long it takes to move a tile (in seconds).")]
    public float movementTime = 1;
    [Tooltip("How long it takes to make a single turn (in seconds). 90 degree and 180 degree turns take the same amount of time.")]
    public float turningTime = 1;

    private State state;
    private Transform transform;

    private float startTime;
    private Vector3 startPosition;
    private Vector3 targetPosition;
    private Quaternion startRotation;
    private Quaternion targetRotation;

    void Start()
    {
        transform = GetComponent<Transform>();
    }

    void Update()
    {
        if (state == State.Moving)
        {
            // Update position.
            float progress = (Time.time - startTime) * movementTime;
            transform.position = Vector3.Lerp(startPosition, targetPosition, Mathf.Min(1, progress));
            if (progress >= 1)
            {
                state = State.Idle;
            }
        }

        if (state == State.Turning)
        {
            // Update rotation.
            float progress = (Time.time - startTime) * turningTime;
            transform.rotation = Quaternion.Lerp(startRotation, targetRotation, Mathf.Min(1, progress));
            if (progress >= 1)
            {
                // Move forward after the player finishes turning.
                MoveOneUnit(0);
            }
        }
    }

    public State GetState()
    {
        return state;
    }

    /// <summary>
    /// Returns a boolean indicating if the player can move 1 unit in the specified direction without colliding with a solid object.
    /// </summary>
    /// <param name="angle">The angle of movement (in degrees) relative to the direction the player is facing.</param>
    /// <returns>A boolean indicating if the player can move 1 unit in the specified direction without colliding with a solid object.</returns>
    public bool CanMove(float angle)
    {
        // The absolute angle.
        float absAngle = transform.rotation.eulerAngles.y + angle;
        Vector3 vec = new Vector3(Mathf.Sin(Mathf.Deg2Rad * absAngle), 0, Mathf.Cos(Mathf.Deg2Rad * absAngle));
        Vector3 position = transform.position;
        return !Physics.Raycast(position, vec, 1);
    }

    /// <summary>
    /// Moves the player a single unit in the specified direction. This method will not make any collision checks.
    /// </summary>
    /// <param name="angle">The angle of movement (in degrees) relative to the direction the player is facing.</param>
    public void MoveOneUnit(float angle)
    {
        // ReSharper disable once CompareOfFloatsByEqualityOperator
        if (angle != 0)
        {
            // Turn the player.
            state = State.Turning;
            startTime = Time.time;
            startRotation = transform.rotation;
            Vector3 targetVector = transform.rotation.eulerAngles + new Vector3(0, angle, 0);
            targetRotation = Quaternion.Euler(targetVector);
        }
        else
        {
            // Move the player.
            state = State.Moving;
            startTime = Time.time;
            startPosition = transform.position;
            float direction = transform.rotation.eulerAngles.y;
            // A vector 1 unit from the current position in the direction the player is facing.
            Vector3 vec = new Vector3(Mathf.Sin(Mathf.Deg2Rad * direction), 0, Mathf.Cos(Mathf.Deg2Rad * direction));
            targetPosition = startPosition + vec;
        }
    }

    public enum State
    {
        Idle,
        Moving,
        Turning
    }
}
