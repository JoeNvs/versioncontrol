﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PredictedDamage : MonoBehaviour {

    int txt;

    private void Start()
    {
        txt = 0;
    }

    private void Update()
    {
        txt = 3 * transform.root.GetComponentsInChildren<EnemyObject>().Length; ;
        GetComponent<Text>().text = "You will take " + txt + " damage this turn";
    }
}
