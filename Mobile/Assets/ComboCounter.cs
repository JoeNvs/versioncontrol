﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*JoseLuis Neves
 * 02/04/19
 * For testing
 * */
public class ComboCounter : MonoBehaviour {

    public int combo;

    private void Start()
    {
        combo = 0;
    }

    private void Update()
    {
        combo = transform.root.GetComponent<BattleClass>().GetCombo();
        GetComponent<Text>().text = "X" + combo;
    }
}
