﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayMemberHp : MonoBehaviour {

    private void Update()
    {
        //Set on screen text member to display hp
        GetComponent<Text>().text = "" + DataController.member.GetHp();
        //
    }
}
