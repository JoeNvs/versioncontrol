﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayManaPool : MonoBehaviour {
    int mana;

    private void Start()
    {
        mana = 0;
    }

    private void Update()
    {
        mana = transform.root.GetComponentInChildren<BattleClass>().GetManaPool();
        GetComponentInChildren<Text>().text = "" + mana;
    }
}
